package proxy;

import java.net.URI;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.MessageEvent;

public class NonParametrizedProxy implements EventHandler {

	// records sent by this producer will have no key, and a string value
	private KafkaProducer<Void, String> kafkaProducer;

	public static void main(String[] args) {
		new NonParametrizedProxy();
	}

	NonParametrizedProxy() {
		try {
			// create the Kafka producer with the appropriate configuration
			kafkaProducer = new KafkaProducer<>(configureKafkaProducer());

			String url = "https://stream.wikimedia.org/v2/stream/recentchange";
			EventSource.Builder builder = new EventSource.Builder(this, URI.create(url));
			EventSource eventSource = builder.build();
			eventSource.start();

			// listen to the stream for 5 seconds
			/*
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			*/
			// listen to the stream for ever
			while (true) { 
				// empty on purpose 
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}

	@Override
	public void onOpen() throws Exception {
		System.out.println("The stream connection has been opened.");
	}

	@Override
	public void onClosed() throws Exception {
		System.out.println("The stream connection has been closed.");
	}

	@Override
	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		String message = messageEvent.getData();
		kafkaProducer.send(new ProducerRecord<Void, String>("wikimedia", null, message));
	}

	@Override
	public void onComment(String comment) throws Exception {
		System.out.println("A comment line (any line starting with a colon) was received from the stream: " + comment);
	}

	@Override
	public void onError(Throwable t) {
		System.out.println("An exception occured on the socket connection: " + t.getMessage());
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		return producerProperties;
	}
}