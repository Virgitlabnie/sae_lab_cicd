package proxy;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.MessageEvent;

public class ProxyThatCounts implements EventHandler {

	private Gson gson;
	private int totalEvents = 0;
	private int wikipediaEvents = 0;

	public static void main(String[] args) {
		new ProxyThatCounts();
	}

	ProxyThatCounts() {
		gson = new Gson();

		String url = "https://stream.wikimedia.org/v2/stream/recentchange";
		EventSource.Builder builder = new EventSource.Builder(this, URI.create(url));
		EventSource eventSource = builder.build();
		eventSource.start();

		// listen to the stream for X seconds
		int sleepingTime = 300;
		try {
			TimeUnit.SECONDS.sleep(sleepingTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// listen to the stream for ever
		/*
		 * while (true) { // empty on purpose }
		 */
		System.out.println("total number of events in " + sleepingTime + " seconds: " + totalEvents);
		System.out.println("total number of Wikipedia events in " + sleepingTime + " seconds: " + wikipediaEvents);
	}

	@Override
	public void onOpen() throws Exception {
		System.out.println("The stream connection has been opened.");
	}

	@Override
	public void onClosed() throws Exception {
		System.out.println("The stream connection has been closed.");
	}

	@Override
	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		totalEvents++;
		
		String message = messageEvent.getData();
		JsonObject jsonObject = gson.fromJson(message, JsonObject.class);
		JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
		String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();

		if (domain.contains("wikipedia")) {
			wikipediaEvents++;
		}
	}

	@Override
	public void onComment(String comment) throws Exception {
		System.out.println("A comment line (any line starting with a colon) was received from the stream: " + comment);
	}

	@Override
	public void onError(Throwable t) {
		System.out.println("An exception occured on the socket connection: " + t.getMessage());
	}
}